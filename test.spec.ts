import {Card, CurrencyEnum} from './index';
import {describe, expect, test} from '@jest/globals';

const card = new Card();
const transactionId1 = card.addTransaction(CurrencyEnum.USD, 50);
card.addTransaction(CurrencyEnum.USD, 45);
card.addTransaction(CurrencyEnum.UAH, 80);

describe('Balances', () => {
    test('balanceUAH', () => {
        expect(card.getBalance(CurrencyEnum.UAH)).toBe(80)
    });
    test('balanceUSD', () => {
        expect(card.getBalance(CurrencyEnum.USD)).toBe(95)
    })
})

describe('Transactions', () => {
    test('found transaction1', () => {
        const foundTransaction1 = card.getTransaction(transactionId1);
        expect(foundTransaction1.amount).toBe(50)
        expect(foundTransaction1.currency).toBe(CurrencyEnum.USD)
    });
    test('add transaction', () => {
        expect(card.transactions.length).toBe(3)
        card.addTransaction(CurrencyEnum.UAH, 100)
        expect(card.transactions.length).toBe(4)
    })
})
